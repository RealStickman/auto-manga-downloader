# Gallery-dl usage
Download certain language and chapter range  
`gallery-dl --chapter-filter "lang == 'en' and 10 <= chapter < 20" "https://mangadex.org/title/296cbc31-af1a-4b5b-a34b-fee2b4cad542/"`  

Set download directory  
`gallery-dl -d (directory)`  
Example directory tree  
```
(directory)/
(directory)/mangasee/
(directory)/mangadex/
```

## Oshi no Ko
https://mangadex.org/title/296cbc31-af1a-4b5b-a34b-fee2b4cad542/

## Land of the Lustrous
### Mangasee
https://mangasee123.com/manga/Houseki-No-Kuni
RSS Feed  
https://mangasee123.com/rss/Houseki-No-Kuni.xml
Direct link to chapter (available in RSS feed)  
https://mangasee123.com/read-online/Houseki-No-Kuni-chapter-95-page-1.html

## Fate/Stay Night - Heaven's Feel
### Mangasee
https://mangasee123.com/manga/Fate-Stay-Night-Heavens-Feel

### Mangadex
If volume is unknown, volume = 0  
https://mangadex.org/title/cade38b7-64c4-4a29-8e3c-8c283291d6c6
Direct link to chapter  
https://mangadex.org/chapter/9375e50e-984c-4511-a1b6-9ed6fce758fe

TODO Search remote for URLs and download  
`gallery-dl "r:https://pastebin.com/raw/FLwrCYsT"`  

It's possible to replace chapters downloaded with empty text files  

## Formatting
https://github.com/mikf/gallery-dl/blob/master/docs/formatting.md  

## Config file
https://github.com/mikf/gallery-dl/blob/master/docs/gallery-dl-example.conf  
https://github.com/mikf/gallery-dl/blob/master/docs/configuration.rst  

### config.json
```
{
	"extractor":
	{
		"mangadex":
		{
			"directory": ["mangadex","{manga}","{volume:?Vol. / /}Chapter {chapter}{chapter_minor:?//}"],
			"filename": "page{page}.{extension}"
		},
		"mangasee":
		{
			"directory": ["mangasee","{manga}","Chapter {chapter}{chapter_minor:?//}"],
			"filename": "page{page}.{extension}"
		}
	}
}
```

# Webp conversion
https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#webp  

https://www.tutorialexample.com/best-practice-to-python-convert-png-to-webp-with-pillow-python-pillow-tutorial/  

# Mangadex Chapter feed
API python wrapper  
https://github.com/EMACC99/mangadex  
*There's currently a syntax error with the module. See this issue for a solution:*  
https://github.com/EMACC99/mangadex/issues/13  

API documentation  
https://api.mangadex.org/docs.html#operation/get-user-follows-manga-feed  

```
>>> import mangadex
>>> api = mangadex.Api()
>>> api.view_manga_by_id(manga_id = "296cbc31-af1a-4b5b-a34b-fee2b4cad542") # general infos
>>> api.get_manga_volumes_and_chapters(manga_id = "cade38b7-64c4-4a29-8e3c-8c283291d6c6") # chapter list
>>> api.get_chapter("75a66c4f-c0ca-43f2-b7b9-3a27d7b70d3d") # specific chapter info
```

# Mangasee Chapter feed
Supports RSS  

RSS content  
`requests.get(url).content`  

RSS with feedparser  
*Blocked by mangasee for some reason*  
`feedparser.parse(url/file)`  
