#!/usr/bin/env python3

# directory and file manipulation
import os

# regex stuff
import re

# encoding and decoding of configured urls
import json

# executing gallery-dl
import subprocess

# downloading mangasee rss feed
import requests

# parsing rss feed from mangasee
import feedparser

# KeysView is for type dict_keys
from typing import Dict, List, Any, KeysView

# config and cache directory
from xdg.BaseDirectory import xdg_config_home, xdg_cache_home

# mangadex api wrapper
import mangadex

# terminal colors
CRED = "\033[91m"
CYEL = "\033[33m"
CGRE = "\033[92m"
CEND = "\033[0m"


# parse user-dirs.dirs file to find download directory
def find_download_dir():
    # NOTE installations without xdg-user-dirs will use a "manga-watcher" directory in the user home directory
    # TODO test whether this works
    # TODO set specific "except"
    try:
        with open(os.path.join(xdg_config_home, "user-dirs.dirs"), "r") as file:
            for line in file:
                if "XDG_DOWNLOAD_DIR=" in line:
                    # match directory path
                    download_list: List[str] = re.findall('".+/.+"', line)
                    download_dir: str = download_list[0]
                    # replace $HOME with real user directory
                    download_dir: str = download_dir.replace(
                        "$HOME", os.environ["HOME"]
                    )
                    # remove quotes around path
                    download_dir: str = download_dir.strip('"')
                    download_dir: str = download_dir + "/manga-watcher"
                    return download_dir
    except:
        download_dir: str = os.path.join(os.path.expanduser("~") + "/manga-watcher")
        return download_dir


# try setting config, cache and download directory
# REVIEW see how this fails if xdg is not installed or XDG_DOWNLOAD_DIR is undefined
config_dir: str = os.path.join(xdg_config_home, "manga-watcher")
cache_dir: str = xdg_cache_home
# download_dir = find_download_dir()
config_file: str = os.path.join(config_dir, "config.json")
gallery_dl_file: str = os.path.join(config_dir, "gallery-dl.json")
downloaded_file: str = os.path.join(config_dir, "downloaded.json")

# create config directory if it doesn't exist yet
if not os.path.isdir(config_dir):
    os.mkdir(config_dir)

# create json file if it doesn't exist yet
# the wanted download targets are already included
# a default download directory is also set
if not os.path.isfile(config_file):
    # FIXME don't ever return None-type
    download_dir: str = find_download_dir()
    # TODO check if the typing could be tightened for this dict
    config_content: Dict[str, Any] = {
        "mangadex": [],
        "mangasee": [],
        "download": download_dir,
        "output": [{"path": "", "type": "kavita"}, {"path": "", "type": "komga"}],
    }
    with open(config_file, "w") as file:
        file.write(json.dumps(config_content, separators=(",", ":"), indent=4))
    print(
        CYEL
        + "First time config file generated. Add links to mangas and optionally change the download directory."
        + CEND
    )
    exit()

# read values from config file
with open(config_file, "r") as file:
    config = json.load(file)

# get download dir from config
download_dir: str = config["download"]

# create desired gallery-dl.json config
# NOTE also replaces () and [] with {} to display it in kavita
# TODO make sure the info.json stuff works with this
# TODO write title also with {}
with open(gallery_dl_file, "w") as file:
    file.write(
        """{
	"extractor":
	{
		"mangadex":
		{
			"directory": ["mangadex","{manga:R(/{/R)/}/R[/{/R]/}/}","[MangaDex] {manga:R(/{/R)/}/R[/{/R]/}/}","{volume:?Vol. / /}Chapter {chapter}{chapter_minor:?//}"],
			"filename": "page{page}.{extension}"
		},
		"mangasee":
		{
			"directory": ["mangasee","{manga:R(/{/R)/}/R[/{/R]/}/}","[MangaSee] {manga:R(/{/R)/}/R[/{/R]/}/}","Chapter {chapter}{chapter_minor:?//}"],
			"filename": "page{page}.{extension}"
		}
	}
}"""
    )

# array to store chapters found for mangadex
mangadex_chapters = []

# array to store chapters found for mangasee
mangasee_chapters = []

########################################
##########      MangaDex      ##########
########################################


# find all chapters for manga from mangadex
def get_mangadex_chapters():
    global mangadex_chapters
    print(CGRE + "Getting chapters from mangadex" + CEND)
    for i in range(0, len(config["mangadex"])):
        url: str = config["mangadex"][i]
        # extract manga id
        manga_list: List[str] = re.findall(
            "[a-z0-9]{8}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{12}", url
        )
        manga: str = manga_list[0]
        manga_info: mangadex.models.Manga = mangadex.Api().view_manga_by_id(
            manga_id=manga
        )
        manga_chapters: Dict[Any, Any] = mangadex.Api().get_manga_volumes_and_chapters(
            manga_id=manga, translatedLanguage=["en"]
        )
        print(CGRE + manga_info.title["en"] + CEND)
        # get all dict keys (volumes)
        # NOTE mangas that aren't available in english will be skipped
        try:
            manga_volumes: KeysView = manga_chapters.keys()
            for volume in manga_volumes:
                # NOTE try to get chapter numbers (array)
                try:
                    chapter_nums: KeysView = manga_chapters[volume]["chapters"].keys()
                    for ch_num in chapter_nums:
                        # print(manga_info.title["en"] + ": Volume " + volume + ", Chapter " + ch_num)
                        chapter_id: str = manga_chapters[volume]["chapters"][ch_num][
                            "id"
                        ]
                        # store chapter id, manga title, manga id, volume and chapter number in dict
                        mangadex_chapters.append(
                            {
                                "id": chapter_id,
                                "title": manga_info.title["en"],
                                "manga_id": manga,
                                "volume": volume,
                                "chapter": ch_num,
                            }
                        )
                # NOTE workaround for manga without chapter numbers
                except AttributeError:
                    chapter_id: str = manga_chapters[volume]["chapters"][0]["id"]
                    # store chapter id, manga title, manga id, volume and chapter number in dict
                    # NOTE specify chapter number as 0 (fixed can't find path for info.json for some manga)
                    mangadex_chapters.append(
                        {
                            "id": chapter_id,
                            "title": manga_info.title["en"],
                            "manga_id": manga,
                            "volume": volume,
                            "chapter": "0",
                        }
                    )
        except AttributeError:
            print(
                CYEL
                + manga_info.title["en"]
                + " is not available in english. Skipping download"
                + CEND
            )


# download chapters from mangadex
def mangadex_dl():
    global mangadex_chapters
    global chapters_downloaded
    print(CGRE + "Downloading new chapters from mangadex" + CEND)
    for chapter in mangadex_chapters:
        manga_id = chapter["manga_id"]
        manga_title = chapter["title"]
        volume = chapter["volume"]
        chapter_num = chapter["chapter"]
        # create the manga id if it doesn't exist yet
        try:
            chapters_downloaded["mangadex"][manga_id]["chapters"]
        except KeyError:
            chapters_downloaded["mangadex"][manga_id] = {
                "title": manga_title,
                "chapters": [],
            }
        if chapter["id"] not in chapters_downloaded["mangadex"][manga_id]["chapters"]:
            print(
                CGRE
                + manga_title
                + ": Volume "
                + volume
                + ", Chapter "
                + chapter_num
                + CEND
            )
            # download chapters not already downloaded
            url = "https://mangadex.org/chapter/" + chapter["id"]
            subprocess.call(
                "gallery-dl -c {} -d {} {}".format(gallery_dl_file, download_dir, url),
                stdout=subprocess.DEVNULL,
                shell=True,
            )
            # FIXME investigate replacing () and [] with {}
            # REVIEW store some info in manga chapter info file
            # NOTE This if handles mangas without chapters and/or volumes
            if volume == "none" and chapter_num != "none":
                infopath = os.path.join(
                    str(download_dir),
                    "mangadex",
                    manga_title,
                    "[MangaDex] " + manga_title,
                    "Chapter " + chapter_num,
                )
            elif volume == "none" and chapter_num == "none":
                infopath = os.path.join(
                    str(download_dir),
                    "mangadex",
                    manga_title,
                    "[MangaDex] " + manga_title,
                    "Chapter 0",
                )
            elif chapter_num != "none":
                infopath = os.path.join(
                    str(download_dir),
                    "mangadex",
                    manga_title,
                    "[MangaDex] " + manga_title,
                    "Vol. " + str(volume) + " Chapter " + chapter_num,
                )
            else:
                infopath = os.path.join(
                    str(download_dir),
                    "mangadex",
                    manga_title,
                    "[MangaDex] " + manga_title,
                    "Vol. " + str(volume) + " Chapter 0",
                )
            # check if chapter path exists
            if os.path.isdir(infopath):
                infofile = os.path.join(infopath, "info.json")
                infocontent = {
                    "manga": manga_title,
                    "manga_id": manga_id,
                    "volume": str(volume),
                    "chapter": chapter_num,
                    "chapter_id": chapter["id"],
                }
                with open(infofile, "w") as file:
                    file.write(json.dumps(infocontent))
            else:
                print(
                    CRED
                    + "Could not find directory for info.json file to go in. Tried path "
                    + infopath
                    + " "
                    + CEND
                )
            # REVIEW ending of new info.json
            # add new chapter to chapters downloaded
            chapters_downloaded["mangadex"][manga_id]["chapters"].append(chapter["id"])
            # NOTE determine whether this really needs to be done right after every download
            # write chapters downloaded into the file
            with open(downloaded_file, "w") as file:
                file.write(
                    json.dumps(chapters_downloaded, separators=(",", ":"), indent=4)
                )


########################################
##########      MangaSee      ##########
########################################


# get mangasee chapters
def get_mangasee_chapters():
    global mangasee_chapters
    print(CGRE + "Getting chapters from mangasee" + CEND)
    for i in range(0, len(config["mangasee"])):
        url: str = config["mangasee"][i]
        # if url isn't directory to the rss feed, change it
        if re.match(".*/manga/.*", url):
            # rss feed url
            url: str = re.sub("/manga/", "/rss/", url) + ".xml"
        # print(url)
        feed_raw: requests.models.Response = requests.get(url)
        # return status code if you don't succeed
        if feed_raw.status_code != 200:
            print(
                CRED
                + "Could not download the RSS feed for "
                + url
                + " Failed with status code "
                + str(feed_raw.status_code)
                + CEND
            )
            # skip further execution with this url
            continue
        # parse rss feed into dict
        feed: feedparser.util.FeedParserDict = feedparser.parse(feed_raw.content)
        # pprint.pprint(feed)
        # manga title
        print(CGRE + feed["feed"]["title"] + CEND)
        # manga chapters (array)
        # pprint.pprint(feed["entries"])
        # get latest chapter
        # print(feed["entries"][0]["link"])
        for chapter in range(0, len(feed["entries"])):
            ch_link: str = feed["entries"][chapter]["link"]
            mangasee_chapters.append(
                {"chapter": ch_link, "title": feed["feed"]["title"]}
            )


# download chapters from mangasee
def mangasee_dl():
    global mangasee_chapters
    global chapters_downloaded
    print(CGRE + "Downloading new chapters from mangasee" + CEND)
    for ch_link in mangasee_chapters:
        # manga chapter
        # print(ch_link["chapter"])
        chapter = ch_link["chapter"]
        # manga title
        manga_title = ch_link["title"]
        # get anglicised japanese title
        manga_alt_list: List[str] = re.findall("read\-online/.*", chapter)
        manga_alt: str = manga_alt_list[0]
        manga_alt = re.sub("read\-online/", "", manga_alt)
        manga_alt = re.sub(
            "\-chapter\-[0-9]+(\.[0-9])?\-page\-[0-9]+\.html", "", manga_alt
        )
        manga_alt = re.sub("\-", " ", manga_alt)
        # create the manga title if it doesn't exist yet
        try:
            chapters_downloaded["mangasee"][manga_title]["chapters"]
            chapters_downloaded["mangasee"][manga_title]["alt_title"]
        except KeyError:
            chapters_downloaded["mangasee"][manga_title] = {
                "alt_title": manga_alt,
                "chapters": [],
            }
        if chapter not in chapters_downloaded["mangasee"][manga_title]["chapters"]:
            chapter_raw_list: List[str] = re.findall("chapter-[0-9\.]+", chapter)
            chapter_raw: str = chapter_raw_list[0]
            chapter_num_list: List[str] = re.findall("[0-9\.]+", chapter_raw)
            # NOTE handle manga without chapter numbers
            chapter_num: str = chapter_num_list[0]
            print(CGRE + manga_title + ": Chapter " + chapter_num + CEND)
            # NOTE replace / with _ in manga directory name
            manga_path = re.sub("/", "_", manga_title)
            # NOTE replace ( and ) with { and }
            manga_path = re.sub("\(", "{", manga_path)
            manga_path = re.sub("\)", "}", manga_path)
            # NOTE replace [ and ] with { and }
            # manga_path = re.sub('\[','{',manga_path)
            # manga_path = re.sub('\]','}',manga_path)
            # download chapter
            subprocess.call(
                "gallery-dl -c {} -d {} {}".format(
                    gallery_dl_file, download_dir, chapter
                ),
                stdout=subprocess.DEVNULL,
                shell=True,
            )
            # REVIEW store some info in manga chapter info file
            # check if chapter path exists
            if os.path.isdir(
                os.path.join(
                    str(download_dir),
                    "mangasee",
                    manga_path,
                    "[MangaSee] " + manga_path,
                    "Chapter " + chapter_num,
                )
            ):
                infofile = os.path.join(
                    str(download_dir),
                    "mangasee",
                    manga_path,
                    "[MangaSee] " + manga_path,
                    "Chapter " + chapter_num,
                    "info.json",
                )
                infocontent = {
                    "manga": manga_title,
                    "manga_alt": manga_alt,
                    "chapter": chapter_num,
                }
                with open(infofile, "w") as file:
                    file.write(json.dumps(infocontent))
            else:
                print(
                    CRED
                    + "Could not find directory for info.json file to go in. Tried path "
                    + os.path.join(
                        str(download_dir),
                        "mangasee",
                        manga_path,
                        "[MangaSee] " + manga_path,
                        "Chapter " + chapter_num,
                        "info.json",
                    )
                    + " "
                    + CEND
                )
            # REVIEW ending of new info.json
            # add chapter to chapters downloaded
            chapters_downloaded["mangasee"][manga_title]["chapters"].append(chapter)
            # NOTE determine whether this really needs to be done right after every download
            # write chapters downloaded into the file
            with open(downloaded_file, "w") as file:
                file.write(
                    json.dumps(chapters_downloaded, separators=(",", ":"), indent=4)
                )


# run function to get manga chapters from mangadex
get_mangadex_chapters()
# print(mangadex_chapters)

# function to get chapters from mangasee
get_mangasee_chapters()
# print(mangasee_chapters)

# load downloaded chapters, or create new file if not found
try:
    with open(downloaded_file, "r") as file:
        chapters_downloaded = json.load(file)
except FileNotFoundError:
    with open(downloaded_file, "w") as file:
        file.write(
            """{
    "mangadex":{

    },
    "mangasee":{

    }
}"""
        )
    # open new file now
    with open(downloaded_file, "r") as file:
        chapters_downloaded = json.load(file)

# download mangadex chapters
mangadex_dl()

# download mangasee chapters
mangasee_dl()

# exit on completion
exit()

# downloaded.json example layout
"""
{
    "mangadex":{
        "3d488908-32c6-4502-be6c-3a7400910cd3":{
            "title":"Blinding Light",
            "chapters":[
                "15c801c4-a480-4e7c-ba46-0fb02f72d3bb",
                "c3459ad4-ccdb-4877-8f46-c766cc1761fb",
                "f62fcb70-3fb9-4390-873f-cf0192a859e0",
                "c6060410-6414-4e89-8d15-278574781d9a",
                "3b4c6134-a67e-4eae-ad4d-1db4f9c51716"
            ]
        }
    },
    "mangasee":{
        "(G) Edition":{
            "alt_title":"G Edition",
            "chapters":[
                "https://mangasee123.com/read-online/G-Edition-chapter-8-page-1.html",
                "https://mangasee123.com/read-online/G-Edition-chapter-7-page-1.html",
                "https://mangasee123.com/read-online/G-Edition-chapter-6-page-1.html",
                "https://mangasee123.com/read-online/G-Edition-chapter-5-page-1.html",
                "https://mangasee123.com/read-online/G-Edition-chapter-4-page-1.html",
                "https://mangasee123.com/read-online/G-Edition-chapter-3-page-1.html",
                "https://mangasee123.com/read-online/G-Edition-chapter-2-page-1.html",
                "https://mangasee123.com/read-online/G-Edition-chapter-1-page-1.html"
            ]
        },
        "Fate/Stay Night - Heaven's Feel":{
            "alt_title":"Fate Stay Night Heavens Feel",
            "chapters":[
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-59-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-58-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-57-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-56-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-55-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-54-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-53-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-52-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-51-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-50-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-49-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-48-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-47-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-46-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-45-page-1.html",
                "https://mangasee123.com/read-online/Fate-Stay-Night-Heavens-Feel-chapter-44-page-1.html"
            ]
        }
    }
}
"""
