#!/usr/bin/env python3

# directory and file manipulation
import os

# regex
import re

# create archives
import shutil

# NOTE possibly move to zipfile in the future
# from zipfile import ZipFile, ZIP_STORED
# loading info.json
import json

# copy stuff to target dirs
from distutils.dir_util import copy_tree

# catch errors from copy_tree
from distutils import errors

# use sleep
import time

# KeysView is for type dict_keys
from typing import List

# multiprocessing
import multiprocessing
from multiprocessing import Pool
from multiprocessing.pool import ThreadPool

# partial definition for functions
from functools import partial

# convert to webp images
from PIL import Image

# config directory
from xdg.BaseDirectory import xdg_config_home

# terminal colors
CRED = "\033[91m"
CYEL = "\033[33m"
CGRE = "\033[92m"
CEND = "\033[0m"


# parse user-dirs.dirs file to find download directory
def find_download_dir():
    # NOTE installations without xdg-user-dirs will use a "manga-watcher" directory in the user home directory
    # TODO test whether this works
    # TODO set specific "except"
    try:
        with open(os.path.join(xdg_config_home, "user-dirs.dirs"), "r") as file:
            for line in file:
                if "XDG_DOWNLOAD_DIR=" in line:
                    # match directory path
                    download_list: List[str] = re.findall('".+/.+"', line)
                    download_dir: str = download_list[0]
                    # replace $HOME with real user directory
                    download_dir: str = download_dir.replace(
                        "$HOME", os.environ["HOME"]
                    )
                    # remove quotes around path
                    download_dir: str = download_dir.strip('"')
                    download_dir: str = download_dir + "/manga-watcher"
                    return download_dir
    except:
        download_dir: str = os.path.join(os.path.expanduser("~") + "/manga-watcher")
        return download_dir


# try setting config, cache and download directory
# TODO see how this fails if xdg is not installed or XDG_DOWNLOAD_DIR is undefined
# download_dir = find_download_dir()
config_dir = os.path.join(xdg_config_home, "manga-watcher")
config_file = os.path.join(config_dir, "config.json")

# output directories are set in config.json
# example configuration
"""
"output":[
    {
        "path":"/home/user/Downloads/kavita",
        "type":"kavita"
    },
    {
        "path":"/home/user/Downloads/komga",
        "type":"komga"
    }
]
"""

# read values from config file
with open(config_file, "r") as file:
    config = json.load(file)

# get download dir from config
download_dir = config["download"]
mangadex_dir = str(download_dir) + "/mangadex"
mangasee_dir = str(download_dir) + "/mangasee"

# get target dirs and put those with a value into target_dirs
output_dirs = config["output"]
target_dirs = []

# only insert non-empty "path" into target_dirs
for i in range(0, len(output_dirs)):
    if len(output_dirs[i]["path"]) != 0:
        target_dirs.append(output_dirs[i])

# exit if no output is specified
if len(target_dirs) == 0:
    print(CYEL + "No output directories defined. Please edit your config file" + CEND)
    exit()


# search files in specified directory with specified extension
def filesearch(extension, directory):
    # print(directory + " " + extension)
    filelist = []
    for dirpath, dirnames, files in os.walk(directory):
        for name in files:
            if extension and name.lower().endswith(extension):
                # print(os.path.join(dirpath, name))
                filelist.append(os.path.join(dirpath, name))
    return filelist


# convert to webp lossless
def convertwebpll(picture):
    # print(picture)
    picture_noext: str = os.path.splitext(picture)[0]
    # open png picture
    image = Image.open(picture)
    # convert image to webp
    image.save(os.path.join(picture_noext + ".webp"), format="WebP", lossless=True)
    image.close
    # remove old image
    os.remove(picture)


# find leaf directories
def findleaf(directory, depth):
    leafdirs = []
    for dirpath, dirnames, files in os.walk(directory):
        if not dirnames and dirpath[len(directory) :].count(os.sep) == depth:
            leafdirs.append(dirpath)
            # print('%s is a leaf' % root)
    return leafdirs


# create zip archive
# REVIEW potentially replace with a more complex zipfile operation to user ZIP_STORED (images won't compress anyways)
# This might speed up execution
# https://stackoverflow.com/a/42056050
def createzip(directory):
    shutil.make_archive(directory, "zip", directory)


# search for png files
pngfiles: List[str] = filesearch(".png", download_dir)


def changeextension(new_extension, filepath):
    # create new filename
    new_filepath: str = os.path.splitext(filepath)[0] + new_extension
    # rename file
    os.rename(filepath, new_filepath)


def removedir(dirpath):
    shutil.rmtree(dirpath)


# get number of cpu threads
# TODO make the number of threads used configurable
cpu: int = multiprocessing.cpu_count()

# NOTE convert pictures to lossless webp
with Pool(cpu) as p:
    # Maybe a process that outputs conversion progress
    p.map(convertwebpll, pngfiles)
    # NOTE it's possible to create multiple pool maps
    # p.map(convertwebpll, pngfiles)

print(CGRE + "Finished converting png to webp" + CEND)

# NOTE depth used to only collect the chapters as leafdirs
# manga-watcher directory structure:
"""
manga-watcher
├── mangadex                           # depth 1
│   ├── Chijou 100-kai                 # depth 2
│   │   ├── [MangaDex] Chijou 100-kai  # depth 3
│   │   │   ├── Vol. 1 Chapter 1       # depth 4
│   │   │   ├── Vol. 1 Chapter 2
│   │   │   ├── Vol. 1 Chapter 3
│   │   │   ├── Vol. 1 Chapter 4
│   │   │   └── Vol. 1 Chapter 5
"""
# depth = 3
depth = 4
leafdirs: List[str] = findleaf(download_dir, depth)
# check length of leafdirs list
if len(leafdirs) == 0:
    print(CYEL + "Couldn't find any chapters to convert" + CEND)

# NOTE convert chapters to zipfile
with Pool(cpu) as p:
    p.map(createzip, leafdirs)

print(CGRE + "Finished creating zipfiles" + CEND)

# NOTE find zipfiles
zipfiles: List[str] = filesearch(".zip", download_dir)

# NOTE change extension of .zip files to .cbz
# NOTE remove unarchived folders
with ThreadPool(cpu) as t:
    t.map(partial(changeextension, ".cbz"), zipfiles)
    t.map(removedir, leafdirs)

print(CGRE + "Renames .zip to .cbz and removed unzipped chapters" + CEND)

# TODO get manga metadata from mangadex
# TODO add ComicInfo.xml to first chapter folder
"""
try:
    with open(infofile,"r") as file:
        info = json.load(file)
        pprint.pprint(info)
        manga = md.Api().view_manga_by_id(manga_id = info["manga_id"])
        author = md.Api().get_author_by_id(author_id = manga.authorId[0])
        artist = md.Api().get_author_by_id(author_id = manga.artistId[0])
        pprint.pprint(manga)
        manga_title = manga.title["en"]
        manga_desc = manga.description["en"]
        manga_year = manga.year
        manga_web = "https://www.mangaupdates.com/series.html?id=" + manga.links["mu"]
        author_name = author.name
        artist_name = artist.name
        print(manga_year)

except FileNotFoundError:
    print("Could not find info.json in path " + infofile)
"""

# go over target directories and move them
# TODO show progress when copying
# FIXME check whether file exists before copying and skip it
for i in range(0, len(target_dirs)):
    target_loc = target_dirs[i]["path"]
    target_type = target_dirs[i]["type"]
    # create output directory if it doesn't exist yet
    if not os.path.isdir(target_loc):
        os.mkdir(target_loc)
    # move manga folder structure
    # NOTE mangadex needs different treatment for komga
    # FIXME handle permission errors
    if target_type == "kavita":
        try:
            copy_tree(mangadex_dir, target_loc)
        except errors.DistutilsFileError:
            pass
    elif target_type == "komga":
        try:
            copy_tree(mangadex_dir, target_loc)
        except errors.DistutilsFileError:
            pass
        # NOTE rename files in target directory
        dirs = os.listdir(target_loc)
        for x in range(0, len(dirs)):
            mangadir = os.path.join(target_loc, dirs[x])
            # lstrip to remove the leading whitespace
            chapters = os.listdir(mangadir)
            for ch in range(0, len(chapters)):
                # FIXME group of None-type
                # FIXME errors for the following manga chapters
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 5 Chapter 36.5 Extra SP05.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 11 Chapter 88.5 Extra SP11.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 4 Chapter 28.5 Extra SP04.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 2 Chapter 13.5 Extra SP02.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 10 Chapter 79.5 Extra SP10.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 3 Chapter 20.5 Extra SP03.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 9 Chapter 70.5 Extra SP09.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 6 Chapter 44.5 Extra SP06.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 8 Chapter 61.5 Extra SP08.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 1 Chapter 6.5 Extra SP01.cbz
                # Error with /mnt/books/komga/Manga/Land of the Lustrous CH: Volume 7 Chapter 52.5 Extra SP07.cbz
                # Error with /mnt/books/komga/Manga/[MANGA Plus] Death Note CH: Death Note Special One-Shot.cbz
                # Error with /mnt/books/komga/Manga/Jumyou wo Kaitotte Moratta. Ichinen ni Tsuki, Ichimanen de. CH: Chapter 16.5
                try:
                    new_name = (
                        re.search("Chapter [0-9]+(\.[0-9])?\.cbz$", chapters[ch])
                        .group(0)
                        .lstrip()
                    )
                except AttributeError:
                    print(
                        CRED + "Error with " + mangadir + " CH: " + chapters[ch] + CRED
                    )
                    # NOTE to fix variable unbound below
                    new_name = mangadir
                os.rename(
                    os.path.join(mangadir, chapters[ch]),
                    os.path.join(mangadir, new_name),
                )
    # copy mangasee
    try:
        copy_tree(mangasee_dir, target_loc)
    except (BrokenPipeError, ConnectionResetError):
        print(CYEL + "Copy error. Retrying in 5 seconds" + CEND)
        time.sleep(5)
        copy_tree(mangasee_dir, target_loc)
    except errors.DistutilsFileError:
        pass

# remove after copying
try:
    shutil.rmtree(mangadex_dir)
except FileNotFoundError:
    pass

try:
    shutil.rmtree(mangasee_dir)
except FileNotFoundError:
    pass

# exit on sucessful completion
exit()

# Target file structure
# Kavita
# TODO the part "Extra SP?" needs to be added
"""
Manga title
    Vol. 1 Chapter 2.5 Extra SP01
    Vol. 2 Chapter 3.5 Extra SP02
    Vol. 1 Chapter 1
    Vol. 1 Chapter 2
    Vol. 2 Chapter 3
    Vol. 3 Chapter 4
    Chapter 5
    ...
"""

# Komga
# NOTE strip Vol. ... from names
"""
Manga title
    Chapter 1
    Chapter 2
    Chapter 3
    Chapter 3.5
    ...
"""
